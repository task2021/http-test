import 'dart:convert';

class Users{
  int id;
  String name;
  int price;
  String about;

  Users({this.id, this.name, this.price, this.about});

  Users.fromJson(Map<String, dynamic> json):
        id = json ["id"],
        name = json ["name"],
        price = json ["price"],
        about = json ["about"];
}