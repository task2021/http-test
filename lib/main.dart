import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app228/model/Users.dart';
import 'package:http/http.dart';
import 'model/Users.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Users> userlist = new List();

  @override
  void initState() {
    // TODO: implement initState
    _getRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lists of Posts"),
      ),
      body: ListView.builder(
          itemCount: userlist.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(userlist[index].name),
              subtitle: Text(userlist[index].about),

            );
          }),

    );
  }

  Future<List<Users>> _getRequest() async {
    Response response =
        await get(Uri.http('192.168.43.248:3000', ''));
    var rb = response.body;
    var list = json.decode(rb) as List;
    List<Users> listvalues = list.map((e) => Users.fromJson(e)).toList();


    setState(() {
      userlist.addAll(listvalues);
    });
  }

}
